require("dotenv").config();
const mongoose = require("mongoose");

const connectDB = async () => {
  try {
    // Database connection 🥳
    await mongoose.connect(process.env.MONGO_CONNECTION_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      // strictQuery: true,
    });

    console.log("DB connected");
  } catch (error) {
    console.log(error);
    throw Error(error);
  }
};

module.exports = connectDB;
