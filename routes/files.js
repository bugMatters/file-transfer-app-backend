require("dotenv").config();
const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const File = require("../models/file");
const { v4: uuid4 } = require("uuid");

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, "uploads/"),
  filename: (req, file, cb) => {
    const uniqueName = `${Date.now()}-${Math.round(
      Math.random() * 1e9
    )}${path.extname(file.originalname)}`;

    cb(null, uniqueName);
  },
});

// const storage = multer.memoryStorage();

let upload = multer({
  storage,
  limit: { fileSize: 1000000 * 100 },
});

router.post("/", upload.single("myFile"), async (req, res) => {
  // Validate request

  console.log(req.file);

  if (!req.file) {
    return res.status(400).json({ error: "All fields are required." });
  }

  //Store into Database

  const file = new File({
    fileName: req.file.filename,
    uuid: uuid4(),
    path: req.file.path,
    size: req.file.size,
  });

  const response = await file.save();

  return res.json({
    file: `${process.env.APP_BASE_URL}/files/${response.uuid}`,
  });
});

router.post("/send", async (req, res) => {
  const { uuid, emailTo, emailFrom } = req.body;
  try {
    if (!uuid || !emailTo || !emailFrom) {
      return res.status(422).send({ error: "All fields are required." });
    }

    // Get data from database
    const file = await File.findOne({ uuid });

    if (file.sender) {
      return res.status(422).send({ error: "Email already sent." });
    }

    file.sender = emailFrom;
    file.receiver = emailTo;
    const response = await file.save();

    // Send email
    const sendMail = require("../services/emailService");

    sendMail({
      from: emailFrom,
      to: emailTo,
      subject: "file sharing",
      text: `${emailFrom} shared a file with you.`,
      html: require("../services/emailTemplate")({
        emailFrom,
        downloadLink: `${process.env.APP_BASE_URL}/files/${file.uuid}`,
        size: parseInt(file.size / 1000) + " KB",
        expires: "24 hours",
      }),
    });

    return res.status(200).send({ success: true });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
