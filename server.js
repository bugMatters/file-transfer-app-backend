require("dotenv").config();
const express = require("express");
const connectDB = require("./config/db");
const path = require("path");
const cors = require("cors");
const app = express();

const PORT = process.env.PORT || 4000;
app.use(cors("*"));
app.use("/file-transfer-api/", express.static("public"));
app.use(express.json());
connectDB();

// Template engine
app.set("views", path.join(__dirname, "/views"));
app.set("view engine", "ejs");

// Routes
app.use("/file-transfer-api/api/files", require("./routes/files.js"));
app.use("/file-transfer-api/files", require("./routes/show"));
app.use("/file-transfer-api/files/download", require("./routes/download"));

app.listen(PORT, () => {
  console.log(`Server running on ${PORT}`);
});
